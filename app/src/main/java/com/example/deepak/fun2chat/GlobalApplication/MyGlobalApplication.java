package com.example.deepak.fun2chat.GlobalApplication;

import android.app.Application;
import com.example.deepak.fun2chat.User.User;
import java.util.ArrayList;

/**
 *
 * This is an application class used to access global variables.
 * It stores the information of user when loggedIn, and
 * server Details containing IpAddress and port number Server.
 *
 * Created by deepak on 12/1/15.
 */
public class MyGlobalApplication extends Application {

    public static final String serverIP = "129.21.22.196"; // IP address of Server Glados
    //public static final String serverIP = "129.21.37.25"; // nevada

    public static final int ServerPortNumber = 12345;
    public static final int MESSAGE_RECEIVER_PORT_NUMBER = 55455;
    public static final String USER_NAME = "userName";


    public String myFirstName = "";
    public String myLastName = "";
    public String myUsername = "";
    public String mySex = "";
    public String myIpAddress = "";
    public String myContactNumber = "";
    public boolean loggedIn = false;
    public ArrayList<User> myContacts =  new ArrayList<>();
    public final Object synchronizedObject = new Object();
    public static final String DATA_SPLITTER = "\\*&\\$#\\^\\*";
    public static final String DATA_SEPARATOR = "*&$#^*";
    public static final String MESSAGE_RECEIVER_ACTION = "MessageReceiverAction";
    public static final String  UPDATE_CHATS_ACTION = "UpdateChatsAction";

}
