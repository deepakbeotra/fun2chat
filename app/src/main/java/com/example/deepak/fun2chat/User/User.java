package com.example.deepak.fun2chat.User;

import java.io.Serializable;

/**
 *
 * This class contains the user information similar to the user class created in the server,
 * to store the info retrieved from server after "GetContacts" request, .
 *
 * Created by deepak on 12/6/15.
 */

public class User implements Serializable {

    private String userName;
    private String firstName;
    private  String lastName;
    private String sex;
    private String contactNumber;

    /**
     * Parameterized Constructor
     *
     * @param userName
     * @param contactNumber
     * @param firstName
     * @param lastName
     * @param sex
     */

    public User(String userName, String contactNumber, String firstName,
                String lastName, String sex) {
        this.userName = userName;
        this.contactNumber = contactNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.sex = sex;
    }

    public String getUserName() {
        return userName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
