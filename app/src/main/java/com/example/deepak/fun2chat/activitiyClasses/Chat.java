package com.example.deepak.fun2chat.activitiyClasses;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.deepak.fun2chat.GlobalApplication.MyGlobalApplication;
import com.example.deepak.fun2chat.R;
import com.example.deepak.fun2chat.database.DatabaseConnector;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/*
 *  This is one of the most important activity class. The subclasses of this class are:
 * MessageCursorAdapter that extends the Simple Cursor Adapter that inflates the chat between two
 * users simultaneously in the list view, DeleteMessage, GetChatForUserFromDatabase,
 * SendMessageToServer which extend AsyncTask  to perform database operations according to their names.
 * This class also contains MessageBroadCastReceiver class which gets broadcast request on receive of
 * message and updates the chat. the list view of the class is registered with the context menu,
 * which provides functionalities to copy and delete, on long click of message.
 * It has an edit text box to write the message, which on hitting the arrow button sends the message
 * to the server. Along with sending text, I was working on the code to send images, but due to
 * limited time couldn’t implement it.
 *
 */

public class Chat extends AppCompatActivity {

    private static final String UTC = "UTC";

    private static final String USER_NAME = "userName";
    private String otherUserName;
    MyGlobalApplication globalApp;
    final private static String ERROR_SENDING = "ERROR SENDING";
    final private static String SENT = "Sent";

    InputMethodManager inputMethodManager;
    ListView messagesListView;
    private EditText editTextChat;

    private MessageCursorAdapter messageCursorAdapter;
    private MessageBroadCastReceiver messageBroadCastReceiver;
    private NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        globalApp = (MyGlobalApplication) getApplication();
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        editTextChat = (EditText) findViewById(R.id.editTextChat);

        messagesListView = (ListView) findViewById(R.id.individualChatsListView);
        notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
        messageBroadCastReceiver = new MessageBroadCastReceiver();

        Intent intent = getIntent();
        otherUserName = intent.getStringExtra(USER_NAME);
        setTitle(otherUserName);

        messageCursorAdapter = new MessageCursorAdapter(Chat.this);
        messagesListView.setAdapter(messageCursorAdapter);
        registerForContextMenu(messagesListView);
        loadChatsForUser(otherUserName);
        hideKeypad(messagesListView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    private void loadChatsForUser(String userName) {
        new GetChatsForUserFromDatabase().execute(userName);

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        registerReceiver(messageBroadCastReceiver, new IntentFilter(globalApp.MESSAGE_RECEIVER_ACTION));
    }
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        unregisterReceiver(messageBroadCastReceiver);
    }

    private void hideKeypad(View view) {
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.message_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if(item.getTitle().equals("Copy")) {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
           // long position = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position;

            View view = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).targetView.findViewById(R.id.messageTextView);

            String selectedText =  ((TextView) view).getText().toString();
            clipboard.setText(selectedText);

        } else if(item.getTitle().equals("Delete")) {
            Toast.makeText(Chat.this, ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).id + "", Toast.LENGTH_LONG).show();
            Long messageId = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).id;
            new DeleteMessage().execute(messageId);
        }
        return super.onContextItemSelected(item);
    }

    /*
        This is on click task to open camera
     */
    public void openCamera(View view) {

        Intent in= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(in, 101);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        // TODO Auto-generated method stub
        if(requestCode==101 && resultCode==RESULT_OK)
        {

        }

    }
    public void sendMessage(View view) {
        hideKeypad(view);
        String message = editTextChat.getText().toString();
        if(!message.toString().trim().equals("")) {
            editTextChat.setText("");
            new SendMessageToServer().execute(otherUserName, globalApp.myUsername, globalApp.myFirstName, message);
        }
    }

    /*
         Async task to delete individual message
     */

    private class DeleteMessage extends AsyncTask<Long, Void, Void> {
        DatabaseConnector databaseConnector = new DatabaseConnector(Chat.this);
        @Override
        protected Void doInBackground(Long... params) {
            synchronized (globalApp.synchronizedObject) {
                databaseConnector.open();
                databaseConnector.deleteMessage(params[0]);
                databaseConnector.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // notify adapter
            new GetChatsForUserFromDatabase().execute(otherUserName);

        }
    }

    /*
         Async Task to send chat message to user
     */
    private class SendMessageToServer extends AsyncTask<String, Object, String> {

        // perform database access
        DatabaseConnector databaseConnector = new DatabaseConnector(Chat.this);
        // return type should match third type in AsyncTask

        String toUserName;
        String fromUserName;
        String fromName;
        String message;
        @Override
        protected String doInBackground(String... params) {

            toUserName = params[0];
            fromUserName = params[1];
            fromName = params[2];
            message = params[3];
            message = setSpecialCharactersOfMessageIfAny(message);
            String sendPacket = toUserName + globalApp.DATA_SEPARATOR + fromUserName + globalApp.DATA_SEPARATOR
                    + fromName + globalApp.DATA_SEPARATOR + message;
            String data = null;
            Socket sc = null;
            DataInputStream dis = null;
            DataOutputStream dos = null;
            try {
                sc = new Socket(globalApp.serverIP, globalApp.ServerPortNumber);
                dos = new DataOutputStream(sc.getOutputStream());// connecting DOS to socket to write into network
                dos.writeBytes("SendMessage\r\n");
                dos.writeBytes(sendPacket + "\r\n");
                dis = new DataInputStream(sc.getInputStream());
                data = dis.readLine(); //"MessageReceivedAtServer"
                Log.d("Chat", " Received message from server " + data);
            } catch (IOException e) {

            } finally {
                try {
                    if (dis != null) {
                        dis.close();
                    }
                    if (dos != null) {
                        dos.close();
                    }
                    if (sc != null) {
                        sc.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }

                // storing message into database
                synchronized (globalApp.synchronizedObject) {
                    databaseConnector.open();
                    String status = null;
                    if (data == null) {
                        status = ERROR_SENDING;
                    } else {
                        status = SENT;
                    }
                    databaseConnector.insertChat(toUserName, fromUserName, "", fromName, toUserName, message, status);
                    databaseConnector.close();
                }
                sendBroadcast(new Intent(globalApp.UPDATE_CHATS_ACTION));
                return data;
            }
        }

        private String setSpecialCharactersOfMessageIfAny(String message) {
            message = message.replaceAll("\\r", "\\\\r");
            message = message.replaceAll("\\n", "\\\\n");
            message = message.replaceAll("\\t", "\\\\t");
            return message;
        }

        @Override
        protected void onPostExecute(String result) {
            String status = null;
            if(result == null) {
                status = ERROR_SENDING;
            } else {
                status = SENT;
            }
            Toast.makeText(getApplicationContext(), status, Toast.LENGTH_SHORT).show();
            // notify adapter
            new GetChatsForUserFromDatabase().execute(otherUserName);

        }
    }

    /*
        Async task to get chats for the user from database
     */
    private class GetChatsForUserFromDatabase extends AsyncTask<String,Void, Cursor> {
        DatabaseConnector databaseConnector = new DatabaseConnector(Chat.this);

        @Override
        protected Cursor doInBackground(String... params) {
            synchronized (globalApp.synchronizedObject) {
                databaseConnector.open();
               return databaseConnector.getChatForUser(params[0]);
            }
        }

        @Override
        protected void onPostExecute(Cursor result) {
            synchronized (globalApp.synchronizedObject) {
                if (databaseConnector != null && databaseConnector.isOpen()) {
                    if (result != null && result.getCount() >= 0) {
                        Log.d("Chat", "num of messages: " + result.getCount());
                        messageCursorAdapter.changeCursor(result);
                    }
                    databaseConnector.close();
                }
            }
        }
    }

    /**
     *
     * Message cursor adapter to inflate chats
     */

    class MessageCursorAdapter extends SimpleCursorAdapter {

        public MessageCursorAdapter(Context context) {
            super(context, -1, null, new String[0], new int[0], 0);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            return  inflater.inflate(R.layout.message_layout, null);
        }


        @Override
        public void bindView(View view, Context context, Cursor cursor) {


            super.bindView(view, context, cursor);


            TextView nameTextView = (TextView) view.findViewById(R.id.nameTextView);
            TextView messageTextView = (TextView) view.findViewById(R.id.messageTextView);

            TextView dateTimeTextView = (TextView) view.findViewById(R.id.dateTimeTextView);
            TextView messageStatusTextView = (TextView) view.findViewById(R.id.messageStatusTextView);
            LinearLayout messageLinearLayout = (LinearLayout)view.findViewById(R.id.messageLinearLayout);
            LinearLayout messageTemplateLinearLayout = (LinearLayout)view.findViewById(R.id.messageTemplateLinearLayout);

            String senderUserName = cursor.getString(cursor.getColumnIndex("fromUserName"));

            nameTextView.setText(cursor.getString(cursor.getColumnIndex("fromName")));
            messageTextView.setText(cursor.getString(cursor.getColumnIndex("message")));
            dateTimeTextView.setText(cursor.getString(cursor.getColumnIndex("created_at")) + UTC);

            if( senderUserName.equals(globalApp.myUsername ) ){
                messageTemplateLinearLayout.setGravity(Gravity.RIGHT);
                messageLinearLayout.setGravity(Gravity.RIGHT);
                messageLinearLayout.setPadding(100, 5, 0, 0);

                messageStatusTextView.setText(cursor.getString(cursor.getColumnIndex("status")));


                nameTextView.setTextColor(getResources().getColor(R.color.orange));
                dateTimeTextView.setTextColor(getResources().getColor(R.color.orange));

            } else {
                messageTemplateLinearLayout.setGravity(Gravity.LEFT);
                messageLinearLayout.setGravity(Gravity.LEFT);
                messageLinearLayout.setPadding(0, 5, 100, 0);
                messageStatusTextView.setText("");
                nameTextView.setTextColor(getResources().getColor(R.color.blue));
                dateTimeTextView.setTextColor(getResources().getColor(R.color.blue));
            }
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }
    }

    /**
     * BraodCast Reciever to update Chats
     */
    class MessageBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            loadChatsForUser(otherUserName);
            notificationManager.cancelAll();
        }
    }


}
