package com.example.deepak.fun2chat.activitiyClasses;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.deepak.fun2chat.GlobalApplication.MyGlobalApplication;
import com.example.deepak.fun2chat.R;
import com.example.deepak.fun2chat.User.User;
import com.example.deepak.fun2chat.fragments.TabsHolderAdapter;
import com.example.deepak.fun2chat.service.MessageReceiverService;

/*
 * This is the main class that is loaded when the user logs in. It comprises of logout and profile
 * view option at the action bar. It also consists of two tabs, Chats and Contacts.
 */
public class Fun2ChatActivity extends AppCompatActivity {

    Fragment chatsFragment;
    Fragment contactsFragment;
    MyGlobalApplication globalApplication;

    private static final String USER_DETAILS = "userDetails";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fun2_chat);
        TabLayout fragmentTabLayout = (TabLayout)findViewById(R.id.fragment_tab_layout);
        fragmentTabLayout.addTab(fragmentTabLayout.newTab().setText("Chats"));
        fragmentTabLayout.addTab(fragmentTabLayout.newTab().setText("Contacts"));
        fragmentTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        globalApplication = (MyGlobalApplication) getApplication();
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.app_logo);
        final ViewPager fragmentViewer = (ViewPager) findViewById(R.id.fragmentViewer);

        TabsHolderAdapter tabsHolderAdapter = new TabsHolderAdapter(getSupportFragmentManager(), fragmentTabLayout.getTabCount(),
                globalApplication , getApplicationContext());

        fragmentViewer.setAdapter(tabsHolderAdapter);

        fragmentViewer.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(fragmentTabLayout));

        fragmentTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                fragmentViewer.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_fun2_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.profile:
                openProfile();
                return true;
            case R.id.logout:
                globalApplication.loggedIn = false;
                startActivity(new Intent(getApplicationContext(),Login.class));
                finish();
             //   new LogOut().execute();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }



    private void openProfile() {
        Intent intent = new Intent(getApplicationContext(), UserDetails.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(USER_DETAILS, new User(globalApplication.myUsername, globalApplication.myContactNumber,
                globalApplication.myFirstName, globalApplication.myLastName, globalApplication.mySex));
        startActivity(intent);
    }
}
