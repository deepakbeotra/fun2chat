package com.example.deepak.fun2chat.activitiyClasses;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;

import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.view.View;
import android.text.format.Formatter;

import android.widget.TextView;

import android.widget.Toast;

import com.example.deepak.fun2chat.GlobalApplication.MyGlobalApplication;
import com.example.deepak.fun2chat.R;
import com.example.deepak.fun2chat.service.MessageReceiverService;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


/*
 * The users have to enter his username and password, if found in the server, the user is logged in
 * and redirected to Fun2ChatActivity, otherwise a toast is displayed stating invalid user details.
 * If it’s a new user, he/she can create an account by clicking on sign up option.
 */
public class Login extends AppCompatActivity {

    DataInputStream dis;
    DataOutputStream dos;
    MyGlobalApplication globalApp;
    private EditText userNameEditText;
    private EditText passwordEditText;
    private TextView errorMessageTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        globalApp = (MyGlobalApplication) getApplication();
        setContentView(R.layout.activity_login);

        if (globalApp.loggedIn) {
            finish();
            startActivity(new Intent(getApplicationContext(), Fun2ChatActivity.class));
        } else {
            userNameEditText = (EditText) findViewById(R.id.usernameEditText);
            passwordEditText = (EditText) findViewById(R.id.passwordEditText);
            errorMessageTextView = (TextView) findViewById(R.id.errorMessageTextView);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void login(View v) {
        String username = userNameEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        if(!(username.equals("") || password.equals(""))) {
            errorMessageTextView.setText("");
            new LoginToServer().execute(username, password);
        } else {
            errorMessageTextView.setText("Invalid User Details");
        }
    }

    public void signup(View v) {
        startActivity(new Intent(this,Signup.class));
    }

    class LoginToServer extends android.os.AsyncTask<String, Integer, String> {


        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            String data = null;
            Socket sc = null;
            try {
                sc = new Socket(globalApp.serverIP, 12345);
                dos = new DataOutputStream(sc.getOutputStream());// connecting DOS to socket to write into network
                dos.writeBytes("Login\r\n");
                dis = new DataInputStream(sc.getInputStream());// connecting DIS to socket for reading from network


                WifiManager wm=(WifiManager)getSystemService(WIFI_SERVICE);
                String ipAddress= Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                globalApp.myIpAddress = ipAddress;
                globalApp.myUsername = params[0];
                //   username, password, ipaddress
                dos.writeBytes(params[0] + globalApp.DATA_SEPARATOR + params[1] + globalApp.DATA_SEPARATOR
                        + globalApp.myContactNumber + globalApp.DATA_SEPARATOR + ipAddress+"\r\n");

                dis= new DataInputStream(sc.getInputStream());// connecting DIS to socket for reading from network

                data= dis.readLine(); //"Registered"
                Log.d("MyMessage", "recieved: " + data);

            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
           //     e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
           //     e.printStackTrace();
            } finally {
                try {
                    if (dis != null) {
                        dis.close();
                    }
                    if (dos != null) {
                        dos.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return data;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {


        }

        private boolean isMyServiceRunning(Class<?> serviceClass) {
            ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(String result) {


            if(result == null) {
                Toast.makeText(getApplicationContext(), "Problem with connection!! Try Again... ", Toast.LENGTH_LONG).show();

            } else if ( result.equals("InvalidEntry")){
                Toast.makeText(getApplicationContext(), "Invalid User Details!!!", Toast.LENGTH_LONG).show();

                errorMessageTextView.setText("Invalid User Details");
            } else {
               // StringTokenizer st = new StringTokenizer(result, globalApp.DATA_SEPARATOR);
                String [] dataValues = result.split(globalApp.DATA_SPLITTER);
                globalApp.myFirstName = dataValues[0];//st.nextToken();
                globalApp.myLastName = dataValues[1];//st.nextToken();
                globalApp.myContactNumber = dataValues[2];//st.nextToken();
                globalApp.mySex = dataValues[3];//st.nextToken();
                globalApp.loggedIn = true;

//                startService(new Intent(Login.this, MessageReceiverService.class));

                Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_LONG).show();

                if(!isMyServiceRunning(MessageReceiverService.class)) {
                    startService(new Intent(getApplicationContext(), MessageReceiverService.class));
                }

                finish();
                startActivity(new Intent(getApplicationContext(), Fun2ChatActivity.class));
            }
        }
    }
}
