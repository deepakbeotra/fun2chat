package com.example.deepak.fun2chat.activitiyClasses;

import android.content.Intent;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.deepak.fun2chat.GlobalApplication.MyGlobalApplication;
import com.example.deepak.fun2chat.R;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
/*
 * The user is asked to fill his personal details, password, username and email id.
 * All these fields are compulsory to create an account successfully. After clicking on register button,
 * user will be again redirected to the login page and would be asked to enter the details. Following regex
 * are used to check the validity of details entered.
 */

public class Signup extends AppCompatActivity {

    MyGlobalApplication globalApp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        globalApp = (MyGlobalApplication) getApplication();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_signup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void register(View v) {
        EditText firstNameEditText = (EditText)findViewById(R.id.firstNameEditText);
        EditText lastNameEditText = (EditText)findViewById(R.id.lastNameEditText);
        EditText usernameEditText = (EditText)findViewById(R.id.usernameEditText);
        RadioGroup radioSexGroup = (RadioGroup) findViewById(R.id.sexRadioGroup);
        RadioButton radioSexButton;

        EditText passwordEditText = (EditText)findViewById(R.id.passwordEditText);
        EditText confirmPasswordEditText = (EditText)findViewById(R.id.confirmPasswordEditText);
        EditText contactNumberEditText = (EditText)findViewById(R.id.contactNumberEditText);
        TextView errorMessageTextView = (TextView)findViewById(R.id.errorMessageTextView);
        boolean isValid = true;
        if(firstNameEditText.getText().toString().length()>0 && !firstNameEditText.getText().toString().matches("[A-Za-z]+")) {
            isValid = false;
            errorMessageTextView.setText("Insert valid First Name.");

        }
        if(isValid && lastNameEditText.getText().toString().length() > 0 && !lastNameEditText.getText().toString().matches("[A-Za-z]+")) {
            isValid = false;
            errorMessageTextView.setText("Insert valid Last Name.");
        }
        if(isValid && (!usernameEditText.getText().toString().matches("[A-Za-z0-9]+[_]*") ||
                !(usernameEditText.getText().toString().length() > 4))) {
            isValid = false;
            errorMessageTextView.setText("Insert valid Username with length > 4");
        }
        if(isValid && (!passwordEditText.getText().toString().matches("[A-Za-z0-9]+") ||
                !(passwordEditText.getText().toString().length() > 5 )) ) {
            isValid = false;
            errorMessageTextView.setText("Insert valid Password with length > 5");
        }
        if(isValid && !passwordEditText.getText().toString().equals(confirmPasswordEditText.getText().toString())) {
            isValid = false;
            errorMessageTextView.setText("Confirm Password doesn't match with Password");
        }
        if(isValid && (contactNumberEditText.getText().toString().length() != 10)) {
            isValid = false;
            errorMessageTextView.setText("Insert valid Contact Number");
        }
        if(isValid) {
            errorMessageTextView.setText("");
            radioSexButton = (RadioButton) findViewById(radioSexGroup.getCheckedRadioButtonId());
            new SendSignUpRequest().execute(firstNameEditText.getText().toString(),lastNameEditText.getText().toString(),
                    usernameEditText.getText().toString(), radioSexButton.getText().toString(),
                    passwordEditText.getText().toString(), contactNumberEditText.getText().toString());
        }
    }

    class SendSignUpRequest extends android.os.AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
            String data = null;
            Socket sc = null;
            DataInputStream dis = null;
            DataOutputStream dos = null;
            try {
                sc = new Socket(globalApp.serverIP, 12345);
                dos = new DataOutputStream(sc.getOutputStream());// connecting DOS to socket to write into network
                dos.writeBytes("RegisterUser\r\n");
                dis = new DataInputStream(sc.getInputStream());// connecting DIS to socket for reading from network


                WifiManager wm=(WifiManager)getSystemService(WIFI_SERVICE);
                String ipAddress= Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                globalApp.myIpAddress = ipAddress;
                globalApp.myFirstName = params[0];
                globalApp.myLastName = params[1];
                globalApp.myUsername = params[2];
                globalApp.mySex = params[3];
                globalApp.myContactNumber = params[5];

                // firstname, lastname, username, sex, password , contact number, ipaddress
                dos.writeBytes(params[0] + globalApp.DATA_SEPARATOR + params[1] + globalApp.DATA_SEPARATOR
                        + params[2] + globalApp.DATA_SEPARATOR + params[3] + globalApp.DATA_SEPARATOR + params[4]
                        + globalApp.DATA_SEPARATOR + params[5] + globalApp.DATA_SEPARATOR + ipAddress + "\r\n");
                    dis= new DataInputStream(sc.getInputStream());// connecting DIS to socket for reading from network

                    data= dis.readLine(); //"Registered"
                    Log.d("MyMessage", "recieved: " + data);

            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (dis != null) {
                        dis.close();
                    }
                    if (dos != null) {
                        dos.close();
                    }
                    if( sc != null) {
                        sc.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return data;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {


        }

        @Override
        protected void onPostExecute(String result) {
            if(result == null) {
                Toast.makeText(getApplicationContext(), "Problem with connection!! Try Again... ", Toast.LENGTH_LONG).show();

            } else if(result.equals("RegisteredSucessfully")) {
                Toast.makeText(getApplicationContext(), "User Registered Successfully", Toast.LENGTH_LONG).show();
                finish();
                startActivity(new Intent(getApplicationContext(), Login.class));
            } else {
                Toast.makeText(getApplicationContext(), result , Toast.LENGTH_SHORT).show();
            }
        }
    }
}
