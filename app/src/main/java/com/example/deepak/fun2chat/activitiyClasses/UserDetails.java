package com.example.deepak.fun2chat.activitiyClasses;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.deepak.fun2chat.R;
import com.example.deepak.fun2chat.User.User;

/*
 * This class is responsible for displaying the details of user selected by the current user.
 */
public class UserDetails extends AppCompatActivity {


    private static final String USER_DETAILS = "userDetails";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        Intent intent = getIntent();
        User user = (User) intent.getSerializableExtra(USER_DETAILS);

        setTitle(user.getUserName());

        TextView firstNameTextView = (TextView)findViewById(R.id.firstNameTextView);
        firstNameTextView.setText(user.getFirstName());
        TextView lastNameTextView = (TextView)findViewById(R.id.lastNameTextView);
        lastNameTextView.setText(user.getLastName());
        TextView sexTextView = (TextView)findViewById(R.id.sexTextView);
        sexTextView.setText(user.getSex());
        TextView contactNumberTextView = (TextView)findViewById(R.id.contactNumberTextView);
        contactNumberTextView.setText(user.getContactNumber());


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
