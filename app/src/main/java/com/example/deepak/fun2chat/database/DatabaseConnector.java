package com.example.deepak.fun2chat.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * This class acts as a middle layer between application and database.
 * It is used for accessing database and performing various tasks on it i.e.
 * Delete Message, Delete chat, Insert message and Fetch Messages.
 * All the operations are performed under object synchronization.
 *
 * Created by deepak on 12/5/15.
 */
public class DatabaseConnector {

    private SQLiteDatabase database; // database object
    private DatabaseOpenHelper databaseOpenHelper; // database helper

    public DatabaseConnector(Context context)  {
        // create a new DatabaseOpenHelper
        databaseOpenHelper =
                DatabaseOpenHelper.getInstance(context);
    }

    public void open() throws SQLException
    {
        // create or open a database for reading/writing
        database = databaseOpenHelper.getWritableDatabase();
    } // end method open

    // close the database connection
    public void close()
    {
        if (database != null)
            database.close(); // close the database connection
    } // end method close

    public void insertChat(String toUserName, String fromUserName, String toName, String fromName,
                          String otherUserName, String message, String status) {
        ContentValues  newChat = new ContentValues();
        newChat.put("toUserName", toUserName);
        newChat.put("fromUserName", fromUserName);
        newChat.put("toName", toName);
        newChat.put("fromName", fromName);
        newChat.put("otherUserName", otherUserName);
        newChat.put("message", message);
        newChat.put("status", status);
      //  newChat.put("created_at", createdAt);

        open(); // open the database
        database.insert("chats", null, newChat);
        close(); // close the database
    } // end method insertChat


    public Cursor getChatForUser(String otherUser) {
        return database.query("chats", new String[] {"_id", "message", "status", "toUserName",
                        "fromUserName", "fromName", "toName", "created_at"}, "toUserName=? or fromUserName=?",
                new String[] { otherUser, otherUser }, null, null, "created_at");
    }



    public Cursor getChats() {
        return database.query("chats", new String[] {"_id","otherUserName","created_at","message"},null, null,
                "otherUserName", null, "created_at DESC");
    }
    // delete the specified message
    public void deleteMessage(long id) {
        database.delete("chats", "_id=" + id, null);
    }

    public void deleteChats(String otherUserName) {
        database.delete("chats", "otherUserName = ?", new String[] { otherUserName});
    }

    public boolean isOpen() {
        return database.isOpen();
    }

}
