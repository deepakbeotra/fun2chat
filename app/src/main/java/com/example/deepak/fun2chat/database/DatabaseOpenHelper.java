package com.example.deepak.fun2chat.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by deepak on 12/11/15.
 *
 *
 * This class creates SQLLite Database in android mobile device, when the application gets executed
 * for the first time. It returns a connection to the created database whenever called. Singleton Design
 * Pattern is used to get the static instance of database in the application.
 *
 */
public class DatabaseOpenHelper extends SQLiteOpenHelper {

    private static DatabaseOpenHelper instance;

    private static final String DATABASE_NAME = "Fun2Chat";

    public static synchronized DatabaseOpenHelper getInstance(Context context) {

        if (instance == null) {
            instance = new DatabaseOpenHelper(context.getApplicationContext(), DATABASE_NAME, null, 1);
        }
        return instance;
    }
    private  DatabaseOpenHelper(Context context, String title,
                                SQLiteDatabase.CursorFactory factory, int version) {
        super(context, title, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createQuery = "CREATE TABLE chats" +
                "(_id integer primary key autoincrement, toUserName TEXT, fromUserName TEXT, " +
                "toName TEXT, fromName TEXT, otherUserName TEXT, message TEXT, status TEXT, created_at DATETIME DEFAULT CURRENT_TIMESTAMP);";
        // execute the query
        db.execSQL(createQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,
                          int newVersion) {
    }
}