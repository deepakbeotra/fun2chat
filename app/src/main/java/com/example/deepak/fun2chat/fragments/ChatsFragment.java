package com.example.deepak.fun2chat.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.deepak.fun2chat.GlobalApplication.MyGlobalApplication;
import com.example.deepak.fun2chat.R;
import com.example.deepak.fun2chat.activitiyClasses.Chat;
import com.example.deepak.fun2chat.database.DatabaseConnector;

/**
 *
 * This class is a fragment class, that displays all the distinct chats of the user with
 * last message and date time stamp. The class contains some sub classes: DeleteChatFromDataBase,
 * GetChatsFromDatabase which extend AsyncTacks to perform database operations and chatsCursorAdapter
 * which extends the Simple Cursor Adapter to inflate chats in the listView, and ChatsBroadCastReceiever
 * to update the chats, on receive of broadcast. Listview is registered with the contextmenu to open
 * chat window for the user on click of “Chat” and delete selected chat on click of “Delete” for the user.

 * Created by deepak on 12/3/15.
 */
public class ChatsFragment extends Fragment {

    private static final String UTC = "UTC";

    private CursorAdapter chatsAdapter;
    static MyGlobalApplication globalApplication;
    static Context applicationContext;
    ListView chatsListView;
    private static ChatsBroadCastReceiver chatsBroadCastReceiver;


    public static ChatsFragment chatsFragmentInstance(MyGlobalApplication globalApp, Context context) {

        globalApplication = globalApp;
        applicationContext = context;
        return new ChatsFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chats,container,false);
        chatsListView = (ListView) view.findViewById(R.id.chatsListView);

        chatsAdapter = new ChatsCursorAdapter(applicationContext);
        chatsListView.setAdapter(chatsAdapter);
        registerForContextMenu(chatsListView);
        chatsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                openChatActivity(((TextView) view.findViewById(R.id.otherUserNameTextView)).getText().toString());
            }
        });
        chatsBroadCastReceiver = new ChatsBroadCastReceiver();
        loadChats();
        return view;
    }

    private void openChatActivity(String otherUserName) {
        Toast.makeText(applicationContext, otherUserName, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(applicationContext, Chat.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(globalApplication.USER_NAME, otherUserName);
        applicationContext.startActivity(intent);
    }


    @Override
    public void onResume() {
        super.onResume();
        loadChats();
        applicationContext.registerReceiver(chatsBroadCastReceiver, new IntentFilter(globalApplication.UPDATE_CHATS_ACTION));
    }

    @Override
    public void onPause() {
        super.onPause();
        applicationContext.unregisterReceiver(chatsBroadCastReceiver);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        Log.d("Chats Fragment", "Chats Context menu title " + ((TextView) v.findViewById(R.id.otherUserNameTextView))
                .getText().toString());
        menu.setHeaderTitle(((TextView) v.findViewById(R.id.otherUserNameTextView)).getText().toString());
        menu.add("Chat");
        menu.add("Delete");
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Log.d("Chats Fragment", "Selected Item:"+ item.getTitle());

        View view  = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).targetView;
        if(view.getId() == R.id.chatLayout) {
            Log.d("Chats Fragment", "Selected Item:"+ item.getTitle());
            String otherUserName = ((TextView) view.findViewById(R.id.otherUserNameTextView)).getText().toString();
            if (item.getTitle().equals("Chat")) {
                openChatActivity(otherUserName);
            } else if (item.getTitle().equals("Delete")) {
                deleteChat(otherUserName);
            }
        }
        return super.onContextItemSelected(item);
    }


    private void loadChats() {
        new GetChatsFromDatabase().execute();
    }

    private void deleteChat(String otherUserName) {
        new DeleteChatsFromDatabase().execute(otherUserName);
    }

    private class DeleteChatsFromDatabase extends AsyncTask<String,Void, Void> {
        DatabaseConnector databaseConnector = new DatabaseConnector(applicationContext);

        @Override
        protected Void doInBackground(String... params) {
            synchronized (globalApplication.synchronizedObject) {
                databaseConnector.open();
                Log.d("Chats", "delete chats for:"+ params[0]);
                databaseConnector.deleteChats(params[0]);
                databaseConnector.close();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            loadChats();
        }
    }


    private class GetChatsFromDatabase extends AsyncTask<String,Void, Cursor> {
        DatabaseConnector databaseConnector = new DatabaseConnector(applicationContext);


        @Override
        protected Cursor doInBackground(String... params) {
            synchronized (globalApplication.synchronizedObject) {
                databaseConnector.open();
                return databaseConnector.getChats();
            }
        }

        @Override
        protected void onPostExecute(Cursor result) {
            synchronized (globalApplication.synchronizedObject) {
                if (databaseConnector != null && databaseConnector.isOpen()) {
                    if (result != null && result.getCount() >= 0) {
                        Log.d("MyMessage", "num of chats: " + result.getCount());
                        chatsAdapter.changeCursor(result);
                    }
                    databaseConnector.close();
                }
            }
        }
    }

    class ChatsCursorAdapter extends SimpleCursorAdapter {

        public ChatsCursorAdapter(Context context) {
            super(context, -1, null, new String[0], new int[0], 0);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            return inflater.inflate(R.layout.chat_layout, null);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            super.bindView(view, context, cursor);

            TextView otherUserNameTextView = (TextView) view.findViewById(R.id.otherUserNameTextView);
            TextView dateTimeTextView = (TextView) view.findViewById(R.id.dateTimeTextView);

            TextView messageTextView = (TextView) view.findViewById(R.id.messageTextView);
            otherUserNameTextView.setText(cursor.getString(cursor.getColumnIndex("otherUserName")));
            dateTimeTextView.setText(cursor.getString(cursor.getColumnIndex("created_at")) + UTC);
            messageTextView.setText(cursor.getString(cursor.getColumnIndex("message")));
        }
    }

     class ChatsBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            loadChats();
        }

    }

}
