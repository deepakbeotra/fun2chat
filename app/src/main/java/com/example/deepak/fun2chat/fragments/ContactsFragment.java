package com.example.deepak.fun2chat.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.format.Formatter;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.deepak.fun2chat.GlobalApplication.MyGlobalApplication;
import com.example.deepak.fun2chat.R;
import com.example.deepak.fun2chat.User.User;
import com.example.deepak.fun2chat.activitiyClasses.UserDetails;
import com.example.deepak.fun2chat.activitiyClasses.Chat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * This class is a fragment class, that displays all the users registered with the server. The class
 * contains some sub classes: FetchContactsFromServer which extends Aync Task to refresh the
 * contacts list, ContactsAdapter which extends BaseAdapter to inflate contacts in the ListView.
 * Listview is registered with the context menu to show the info of selected user on click of “Info”
 * and opens a chat window on click of “Chat”. It contains the feature to refresh contacts on pull.
 *
 * Created by deepak on 12/3/15.
 */
public class ContactsFragment extends Fragment {

    static Socket sc;
    DataInputStream dis;
    DataOutputStream dos;
    ListView contactsListView;
    ContactsAdapter contactsAdapter;
    static ArrayList<User> contacts;
    static MyGlobalApplication globalApplication;
    private SwipeRefreshLayout swipeRefreshContactsLayout;
    static Context applicationContext;

    private static final String USER_DETAILS = "userDetails";

    public static ContactsFragment contactsFragmentInstance(MyGlobalApplication globalApp, Context context) {

        globalApplication = globalApp;
        contacts = globalApp.myContacts;
        applicationContext = context;
        return new ContactsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contacts,container,false);

        contactsListView = (ListView) view.findViewById(R.id.contactsListView);
        contactsAdapter = new ContactsAdapter();
        contactsListView.setAdapter(contactsAdapter);

        if(contacts.size() == 0) {
            loadContacts();
        }

        swipeRefreshContactsLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshContactsLayout);
        swipeRefreshContactsLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light
        );
        swipeRefreshContactsLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        loadContacts();
                    }
                }
        );
        registerForContextMenu(contactsListView);
        contactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                openChatActivity(position);
            }
        });

        return view;
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        Log.d("Contacts Fragment", "Context menu title " + ((TextView) v.findViewById(R.id.contactUserNameTextView))
                .getText().toString());
        menu.setHeaderTitle(((TextView) v.findViewById(R.id.contactUserNameTextView)).getText().toString());
        menu.add("Chat");
        menu.add("Info");
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {

         int position = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position;
        View view  = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).targetView;
        if(view.getId() == R.id.contactLayout) {
            Log.d("Contacts Fragment", "Selected Item:"+ item.getTitle());
            if (item.getTitle().equals("Chat")) {
                openChatActivity(position);
            } else if (item.getTitle().equals("Info")) {
                openContactsDetailsActivity(position);
            }
        }
        return super.onContextItemSelected(item);
    }

    private void openChatActivity(int position) {
        Toast.makeText(applicationContext, contacts.get(position).getFirstName(), Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(applicationContext, Chat.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(globalApplication.USER_NAME, contacts.get(position).getUserName());
        applicationContext.startActivity(intent);
    }

    private void openContactsDetailsActivity( int position) {
        Toast.makeText(applicationContext, contacts.get(position).getFirstName(), Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(applicationContext, UserDetails.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(USER_DETAILS, contacts.get(position));
        applicationContext.startActivity(intent);
    }
    private void loadContacts() {
        new FetchContactsFromServer().execute();
    }

    class ContactsAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return contacts.size();
        }

        @Override
        public Object getItem(int position) {
            return contacts.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = LayoutInflater.from(getActivity());
            convertView = inflater.inflate(R.layout.contact_layout, parent,false);
            TextView contactUserNameTextView = (TextView)convertView.findViewById(R.id.contactUserNameTextView);
            contactUserNameTextView.setText(contacts.get(position).getUserName());
            ImageView contactImageView = (ImageView) convertView.findViewById(R.id.contactImageView);


            if(contacts.get(position).getSex().equals("Male")) {
                contactImageView.setImageResource(R.drawable.ic_male);

            } else {
                contactImageView.setImageResource(R.drawable.ic_female);
            }

            return convertView;
        }
    }

    class FetchContactsFromServer extends android.os.AsyncTask<String, Integer, String> {


        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            String data = null;

            try {

                sc = new Socket(globalApplication.serverIP, globalApplication.ServerPortNumber);
                dos = new DataOutputStream(sc.getOutputStream());// connecting DOS to socket to write into network
                dos.writeBytes("GetContacts\r\n");
                dis = new DataInputStream(sc.getInputStream());// connecting DIS to socket for reading from network


                WifiManager wm = (WifiManager)applicationContext.getSystemService(applicationContext.WIFI_SERVICE);
                String ipAddress= Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                globalApplication.myIpAddress = ipAddress;
                //   sending username, ipaddress
                dos.writeBytes(globalApplication.myUsername + globalApplication.DATA_SEPARATOR + ipAddress + "\r\n");

                dis= new DataInputStream(sc.getInputStream());// connecting DIS to socket for reading from network

                data= dis.readLine(); //"receiving user details"
                contacts = new ArrayList<>();
                if(data != null) {
                    while(!data.equals("UsersUpdated") ) {

                        String [] dataValues = data.split(globalApplication.DATA_SPLITTER);
                        if(!dataValues[0].equals(globalApplication.myUsername)) {
                            // sending  username, contactnumber, firstname, lastname, sex
                            contacts.add(new User(dataValues[0], dataValues[1], dataValues[2],
                                    dataValues[3], dataValues[4]));
                        }
                        data = dis.readLine(); //"user details"
                    }
                    Collections.sort(contacts, new Comparator<User>() {
                        @Override
                        public int compare(User user1, User user2) {
                            if(user1.getUserName().compareTo(user2.getUserName())>=0) {
                                return 1;
                            }
                            return -1;
                        }
                    });
                }
                Log.d("MyMessage", "recieved: " + data);

            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                try {
                    if (dis != null) {
                        dis.close();
                    }
                    if (dos != null) {
                        dos.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return data;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }
        @Override
        protected void onPostExecute(String result) {


            if(result == null) {
                Toast.makeText(applicationContext, "Problem with connection!! Try Again... ", Toast.LENGTH_SHORT).show();

            } else if(result.equals("UsersUpdated")) {
                Toast.makeText(applicationContext, "Contacts Updated " + contacts.size(), Toast.LENGTH_SHORT).show();
            }
            // update the contacts in global app
            contactsAdapter.notifyDataSetChanged();
            globalApplication.myContacts = contacts;
            swipeRefreshContactsLayout.setRefreshing(false);
        }
    }
}
