package com.example.deepak.fun2chat.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.deepak.fun2chat.GlobalApplication.MyGlobalApplication;

/**
 * This class extends the FragmentStatePagerAdapter to give the facility of inflating two fragments
 * and then switching between two fragments. These two fragments are the classes
 * discussed above – ChatFragment and ContactFragment.
 *
 * Created by deepak on 12/3/15.
 */
public class TabsHolderAdapter extends FragmentStatePagerAdapter {

    ChatsFragment chatsTab;
    ContactsFragment contactsTab;
    int tabsNum;

    /**
     * Parameterized Constructor to inflate fragments
     *
     * @param fragmentManager
     * @param tabsNum
     * @param globalApp
     * @param context
     */
    public TabsHolderAdapter(FragmentManager fragmentManager, int tabsNum,  MyGlobalApplication globalApp, Context context) {
        super(fragmentManager);
        chatsTab = ChatsFragment.chatsFragmentInstance(globalApp, context);
        contactsTab = ContactsFragment.contactsFragmentInstance(globalApp, context);
        this.tabsNum = tabsNum;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                return chatsTab;
            case 1:
                return contactsTab;
        }
        return null;
    }

    @Override
    public int getCount() {
        return tabsNum;
    }
}
