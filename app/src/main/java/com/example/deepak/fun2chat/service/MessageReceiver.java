package com.example.deepak.fun2chat.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.util.Log;

import com.example.deepak.fun2chat.GlobalApplication.MyGlobalApplication;
import com.example.deepak.fun2chat.activitiyClasses.Chat;
import com.example.deepak.fun2chat.database.DatabaseConnector;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * This class acts a listener for messages being received from the server.
 * These messages are stored in the database. Once the messages are stored,
 * notifications are created for them and a broadcast request is sent to custom broadcast
 * receivers to update the chat.
 *
 * Created by deepak on 12/10/15.
 */
class MessageReceiver implements Runnable {
    ServerSocket serverSocket;
    Socket socket;

    NotificationManager notificationManager;
    Notification.Builder notificationBuilder;
    Context context;
    int notificationTitlePicId;
    MyGlobalApplication globalApp;


    public MessageReceiver(NotificationManager notificationManager,MyGlobalApplication globalApp,
                           Context context , int notificationTitlePicId) {
        try {
            Log.d("MessageReceiver", "MessageReceiver started");
            this.context = context;
            this.notificationManager = notificationManager;
            this.notificationTitlePicId = notificationTitlePicId;
            this.globalApp = globalApp;
            //  notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            serverSocket = new ServerSocket(globalApp.MESSAGE_RECEIVER_PORT_NUMBER);
            Log.d("MessageReceiver", "MessageReceiverServer started");
        } catch (IOException e) {
        }
    }

    @Override
    public void run() {
        int notificationId = 0;
        while (true) {
            try {
                Log.d("MessageReceiver", "Message arrived");
                    socket = serverSocket.accept();
                    if(globalApp.loggedIn) {
                        new Thread(new MessageReceiverHandler(++notificationId, socket)).start();
                    }
                    else {
                        if(socket != null) {
                            socket.close();
                        }
                    }

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
     }

    private class MessageReceiverHandler implements  Runnable {

        Socket messageSocket;
        int notificationID;
        DataOutputStream dos;
        BufferedReader bufferedReader;

        MessageReceiverHandler(int notificationID, Socket socket) {
            this.notificationID = notificationID;
            messageSocket = socket;
            new Thread(this).start();
        }

        @Override
        public void run() {
            String receivedPacket = "";
            try {
                    Log.e("***", receivedPacket);
                    bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    receivedPacket = new String(bufferedReader.readLine());
                    DataOutputStream dos = new DataOutputStream(socket.getOutputStream());

                    // split the message and store into the database
                    String[] dataValues = receivedPacket.split(globalApp.DATA_SPLITTER);

                    if (dataValues[0].equals(globalApp.myUsername)) {
                        acceptMessage(dataValues);
                        dos.writeBytes("Message Accepted\r\n");
                        shakeDevice();
                    } else {
                        dos.writeBytes("Message Discarded\r\n");
                    }
            }catch(IOException | NullPointerException e){
            }finally{
                try {
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                    if (dos != null) {
                        dos.close();
                    }
                } catch (IOException e) {

                }
            }

        }

        /**
         *
         * This message is used to store the received message into database.
         *
         * @param dataValues
         */
        private void acceptMessage (String[] dataValues) {
            String toUserName = dataValues[0];
            String fromUserName = dataValues[1];
            String fromName = dataValues[2];
            String message = dataValues[3];
            for (int i = 4; i < dataValues.length; ++i) {
                message += globalApp.DATA_SEPARATOR + dataValues[i];
            }

            storeMessageIntoDatabase(toUserName, fromUserName, globalApp.myFirstName, fromName, message);
            generateNotification(message, fromUserName);
            sendBroadCastRequest();
        }

        private void storeMessageIntoDatabase(String toUserName, String fromUserName,
                                              String toName, String fromName, String message) {
            DatabaseConnector databaseConnector = new DatabaseConnector(context);

            synchronized (globalApp.synchronizedObject) {
                databaseConnector.open();
                databaseConnector.insertChat(toUserName, fromUserName, toName, fromName, fromUserName, message, "");
                databaseConnector.close();
            }
        }

        private void generateNotification( String message, String fromUserName) {
            notificationBuilder = new Notification.Builder(context);
            notificationBuilder.setSmallIcon(notificationTitlePicId);
            notificationBuilder.setContentTitle("Message from " + fromUserName);
            notificationBuilder.setContentText(message);
            // to open activity and to pass variables to intent
            Intent intent = new Intent(context, Chat.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(globalApp.USER_NAME, fromUserName);
            PendingIntent pIntent = PendingIntent.getActivity(context, 100,
                    intent, 0);
            if(globalApp.loggedIn) {
                notificationBuilder.setContentIntent(pIntent);
            }
            notificationManager.notify(notificationID, notificationBuilder.build());
        }

        private void shakeDevice() {
            Vibrator vibrate = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            vibrate.vibrate(500);
        }

        private void sendBroadCastRequest() {

            context.sendBroadcast(new Intent(globalApp.MESSAGE_RECEIVER_ACTION));
            context.sendBroadcast(new Intent(globalApp.UPDATE_CHATS_ACTION));
        }
    }

}