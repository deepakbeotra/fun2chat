package com.example.deepak.fun2chat.service;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Toast;

import com.example.deepak.fun2chat.GlobalApplication.MyGlobalApplication;
import com.example.deepak.fun2chat.R;

/**
 *
 * This class extends the Service class. This class is created to implement STICKY_SERVICE.
 * It updates the IpAddress of device saved in the server every minute and initiates a new
 * thread to listen messages being sent by the server.
 *
 * Created by deepak on 12/7/15.
 */
public class MessageReceiverService extends Service {


    MessageReceiver messageReceiver;
    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("MessageReceiverService ", " Service started ");

        Toast.makeText(getApplicationContext(),"MessageReceiverService Started", Toast.LENGTH_SHORT).show();
        if(messageReceiver == null) {
            Log.d("  MessageReceiver ", "  new object ");
            messageReceiver = new MessageReceiver((NotificationManager) getSystemService(NOTIFICATION_SERVICE),
                    (MyGlobalApplication)getApplication(), getApplicationContext(), R.drawable.app_logo);
            new Thread(messageReceiver).start();
        }
        new Thread(new Runnable() {

            @Override
            public void run() {
                while(true) {
                    if(((MyGlobalApplication)getApplication()).loggedIn) {
                        Log.d("MessageReceiverService ", " Trying to  update ipaddress ");
                        try {
                            String ipAddress = Formatter.formatIpAddress(((WifiManager) getSystemService(WIFI_SERVICE))
                                    .getConnectionInfo().getIpAddress());
                            MyGlobalApplication globalApplication = (MyGlobalApplication) getApplication();
                            if( !ipAddress.equals(globalApplication.myIpAddress)) {

                                globalApplication.myIpAddress = ipAddress;
                                new UpdateIPAddress(globalApplication, getApplicationContext()).execute();
                            }
                            Thread.sleep(60000);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }

            }
        }).start();

        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        // TODO Auto-generated method stub
        super.onTaskRemoved(rootIntent);
        startService(new Intent(this, MessageReceiverService.class));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("MessageReceiverService ", " Service stopped ");
    }
}
