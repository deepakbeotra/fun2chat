package com.example.deepak.fun2chat.service;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Toast;

import com.example.deepak.fun2chat.GlobalApplication.MyGlobalApplication;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * This class extends AsyncTask to update the IP address of devices stored in the server.
 *
 * Created by deepak on 12/10/15.
 */

class UpdateIPAddress extends android.os.AsyncTask<Void, Void,String> {

    private static final String NETWORK_PROBLEM_MESSAGE = "Network_Problem";

    MyGlobalApplication globalApp;
    Context context;
    UpdateIPAddress(MyGlobalApplication globalApp, Context context) {
        this.globalApp = globalApp;
        this.context = context;
    }

    @Override
    protected String doInBackground(Void... params) {
        String data = null;
        DataInputStream dis = null;
        DataOutputStream dos = null;
        try {

            Socket sc = new Socket(globalApp.serverIP, globalApp.ServerPortNumber);
            Toast.makeText(context,"Updating Ip Address to Server", Toast.LENGTH_SHORT).show();
            dos = new DataOutputStream(sc.getOutputStream());// connecting DOS to socket to write into network
            dis = new DataInputStream(sc.getInputStream());// connecting DIS to socket for reading from network
            dos.writeBytes("UpdateIpAddress\r\n");
            dos.writeBytes(globalApp.myUsername + globalApp.DATA_SEPARATOR + globalApp.myIpAddress);
            data = dis.readLine(); //"IPAddress Updated"
            Log.d("Update Ipaddress ", "Ip updated " + data);

        } catch ( IOException e) {
            // TODO Auto-generated catch block
            data = NETWORK_PROBLEM_MESSAGE;
        } finally {
            try {
                if (dis != null) {
                    dis.close();
                }
                if (dos != null) {
                    dos.close();
                }
            } catch (IOException e) {

            }
        }
        return data;

    }

    @Override
    protected void onPostExecute(String result) {
        if(result != null && !result.equals(NETWORK_PROBLEM_MESSAGE)) {
            Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
        }
    }

}